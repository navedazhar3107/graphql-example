package com.example.graphqltest;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BookGateway {

    private final BookRepository bookRepository;


    public BookGateway(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public List<Book> getBooks(){
        return (List<Book>) bookRepository.findAll();
    }

    public void createBooks(Book book){
        bookRepository.save(book);
    }
}


