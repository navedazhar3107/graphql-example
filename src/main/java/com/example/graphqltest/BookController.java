package com.example.graphqltest;

import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;


@Controller
public class BookController {

    private final BookGateway bookGateway;


    public BookController(BookGateway bookGateway) {
        this.bookGateway = bookGateway;
    }

    @QueryMapping("allBooks")
    public List<Book> getBooks(){
        return bookGateway.getBooks();
    }

    public ResponseEntity<Book> createBooks(@RequestBody Book book){
        bookGateway.createBooks(book);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
