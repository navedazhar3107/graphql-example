package com.example.graphqltest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphQlTestApplication implements CommandLineRunner {

    @Autowired
    private BookGateway bookGateway;

    public static void main(String[] args) {
        SpringApplication.run(GraphQlTestApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        Book book=new Book();
        book.setId(12L);
        book.setName("First");
        bookGateway.createBooks(book);
    }
}
